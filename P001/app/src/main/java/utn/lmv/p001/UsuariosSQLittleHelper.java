package utn.lmv.p001;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class UsuariosSQLittleHelper extends SQLiteOpenHelper
{
    private String sqlCreateA = "CREATE TABLE Usuarios (codigo INTEGER, nombre TEXT, password TEXT)";
    private String sqlCreateB = "CREATE TABLE Car (codigo_car INTEGER, modelo TEXT)";
    private String sqlCreateC = "CREATE TABLE CarDetail (codigo_car INTEGER, accel TEXT, power TEXT, top_speed TEXT, range TEXT, price TEXT)";

    public UsuariosSQLittleHelper(Context contexto, String nombre, CursorFactory factory, int version)
    {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(sqlCreateA);
        db.execSQL(sqlCreateB);
        db.execSQL(sqlCreateC);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva)
    {
        db.execSQL("DROP TABLE IF EXISTS Usuarios");
        db.execSQL("DROP TABLE IF EXISTS Car");
        db.execSQL("DROP TABLE IF EXISTS CarDetail");
        db.execSQL(sqlCreateA);
        db.execSQL(sqlCreateB);
        db.execSQL(sqlCreateC);
    }
}