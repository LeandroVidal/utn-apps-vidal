package utn.lmv.p001;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoViewAttacher;

public class CarA_Fragment extends Fragment
{
    public TextView carModel;
    public ImageView carImageView;
    public String selectedCar = CarTabsActivity.selectedCar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_car_a, container, false);

        carModel = view.findViewById(R.id.GRAFcarModel);
        carImageView = view.findViewById(R.id.GRAFcarImage);

        PhotoViewAttacher photoView = new PhotoViewAttacher (carImageView);
        photoView.update();

        // Seteo texto con el modelo del auto
        carModel.setText(selectedCar);

        // Seteo imagen del auto
        switch (selectedCar)
        {
            case "Tesla Model 3":
                carImageView.setImageResource(R.drawable.tesla_model_3);
                break;
            case "Tesla Model X":
                carImageView.setImageResource(R.drawable.tesla_model_x);
                break;
            case "Tesla Model S":
                carImageView.setImageResource(R.drawable.tesla_model_s);
                break;
            case "Tesla Roadster":
                carImageView.setImageResource(R.drawable.tesla_roadster);
                break;
            case "Tesla Semi":
                carImageView.setImageResource(R.drawable.tesla_semi);
                break;
            case "Porsche Mission E":
                carImageView.setImageResource(R.drawable.porsche_mission_e);
                break;
            case "Audi E-Tron Sportback":
                carImageView.setImageResource(R.drawable.audi_etron_sportback);
                break;
            case "BMW i3":
                carImageView.setImageResource(R.drawable.bmw_i3);
                break;
            case "Nissan Leaf":
                carImageView.setImageResource(R.drawable.nissan_leaf);
                break;
            case "Renault Twizy Urban 80":
                carImageView.setImageResource(R.drawable.renault_twizy);
                break;
            default:
                carImageView.setImageResource(R.drawable.electric_default);
        }

        return view;
    }
}
