package utn.lmv.p001;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Objects;

public class CarTabsActivity extends AppCompatActivity
{
    public static String selectedCar = "DEFAULT";
    public static ArrayList<String> valuesSpin = new ArrayList<>();

    public ViewPagerAdapter mViewPagerAdapter;
    public ViewPager mViewPager;
    public TabLayout tabLayout;
    //Abrimos la base de datos 'DBUsuarios' en modo escritura
    public UsuariosSQLittleHelper usdbh = new UsuariosSQLittleHelper(this, "DB_LMV", null, 1);

    public CarTabsActivity()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_tabs);

        mViewPagerAdapter = new ViewPagerAdapter( getSupportFragmentManager() );

        mViewPager = findViewById(R.id.user_pager);

        Integer selectedCarCode = Objects.requireNonNull(getIntent().getExtras()).getInt("CAR",0);

        SQLiteDatabase db = usdbh.getWritableDatabase();
        String[] keysSpin = {"0-60mph Acceleration Time","Power","Top Speed","Range","Base Price"}; // Aca poner datos de CarDetail
        valuesSpin = new ArrayList<>();
        //Si hemos abierto correctamente la base de datos
        if(db != null)
        {
            Cursor c = db.rawQuery("SELECT modelo FROM Car WHERE codigo_car=" + selectedCarCode,null);
            if(c.moveToFirst())
            {
                selectedCar = c.getString(0);
            }
            c.close();

            c = db.rawQuery("SELECT accel,power,top_speed,range,price FROM CarDetail WHERE codigo_car=" + selectedCarCode,null);
            if(c.moveToFirst())
            {
                for(int i=0;i<5;i++)
                {
                    valuesSpin.add(keysSpin[i] + ": " + c.getString(i));
                }
            }
            c.close();

            //Cerramos la base de datos
            db.close();
        }

        setupViewPager(mViewPager);

        tabLayout = findViewById(R.id.user_tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter( getSupportFragmentManager() );

        adapter.addFragment(new CarA_Fragment(), "CAR OVERVIEW");
        adapter.addFragment(new CarB_Fragment(), "CAR SPECS");

        viewPager.setAdapter(adapter);
    }
}