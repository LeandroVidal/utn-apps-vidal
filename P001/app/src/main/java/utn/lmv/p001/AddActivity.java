package utn.lmv.p001;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddActivity extends AppCompatActivity
{
    public EditText edtMODEL;
    public EditText edtACCEL;
    public EditText edtPOWER;
    public EditText edtSPEED;
    public EditText edtRANGE;
    public EditText edtPRICE;

    public String model_value = "";
    public String accel_value = "";
    public String power_value = "";
    public String speed_value = "";
    public String range_value = "";
    public String price_value = "";

    public TextView txtCarModel;

    public Button btnAdd;

    //Abrimos la base de datos 'DB_LMV' en modo escritura
    public UsuariosSQLittleHelper usdbh = new UsuariosSQLittleHelper(this, "DB_LMV", null, 1);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        edtMODEL = findViewById(R.id.GRAFEdtMODEL);
        edtACCEL = findViewById(R.id.GRAFEdtACCEL);
        edtPOWER = findViewById(R.id.GRAFEdtPOWER);
        edtSPEED = findViewById(R.id.GRAFEdtSPEED);
        edtRANGE = findViewById(R.id.GRAFEdtRANGE);
        edtPRICE = findViewById(R.id.GRAFEdtPRICE);

        txtCarModel = findViewById(R.id.GRAFtxtView);

        btnAdd = findViewById(R.id.GRAFbtnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SQLiteDatabase db = usdbh.getWritableDatabase();

                model_value = '"' + edtMODEL.getText().toString() + '"';
                accel_value = '"' + edtACCEL.getText().toString() + '"';
                power_value = '"' + edtPOWER.getText().toString() + '"';
                speed_value = '"' + edtSPEED.getText().toString() + '"';
                range_value = '"' + edtRANGE.getText().toString() + '"';
                price_value = '"' + edtPRICE.getText().toString() + '"';

                if( model_value.length() == 2 || accel_value.length() == 2 || power_value.length() == 2 || speed_value.length() == 2 || range_value.length() == 2 || price_value.length() == 2  )
                {
                    Toast.makeText(getApplicationContext(), "All fields must be completed", Toast.LENGTH_SHORT).show();
                }
                else
                {

                    //Si hemos abierto correctamente la base de datos
                    if (db != null)
                    {
                        Integer ultimoAuto = 0;

                        Cursor c = db.rawQuery("SELECT codigo_car FROM Car ORDER BY codigo_car DESC LIMIT 1", null);
                        if (c.moveToFirst())
                        {
                            ultimoAuto = c.getInt(0) + 1;
                            c.close();
                        }

                        db.execSQL("INSERT INTO CarDetail (codigo_car,accel,power,top_speed,range,price) VALUES (" + ultimoAuto + "," +
                                accel_value + "," + power_value + "," + speed_value + "," + range_value + "," + price_value + ")");

                        db.execSQL("INSERT INTO Car (codigo_car,modelo) VALUES (" + ultimoAuto + "," + model_value + ")");

                        //Cerramos la base de datos
                        db.close();
                    }
                    Toast.makeText(getApplicationContext(), "New car successfully added", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}
