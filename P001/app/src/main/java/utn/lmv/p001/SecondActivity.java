package utn.lmv.p001;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

public class SecondActivity extends AppCompatActivity
{
    public Boolean refreshRequired = false;
    public TextView currentUser;
    public String userTexto;
    public ListView listaAutos;
    public FloatingActionButton btnFloatAdd;

    //Abrimos la base de datos 'DB_LMV' en modo escritura
    public UsuariosSQLittleHelper usdbh = new UsuariosSQLittleHelper(this, "DB_LMV", null, 1);

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_settings:
                refreshRequired = true;
                Intent Int2 = new Intent(SecondActivity.this,SettingsActivity.class);
                startActivity(Int2);
                return true;
            case R.id.action_test:
                finish();
                startActivity(getIntent());
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Esto es para forzar a que se regenere el ListView despues de modificar las opciones
    @Override
    protected void onResume()
    {
        super.onResume();

        if (refreshRequired)
        {
            refreshRequired = false;
            finish();
            startActivity(getIntent());
        }
    }

//---ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY---

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Toolbar myToolbar = findViewById(R.id.GRAFtoolbar);
        setSupportActionBar(myToolbar);

        currentUser = findViewById(R.id.GRAFcurrentUser);
        btnFloatAdd = findViewById(R.id.GRAFbtnFloat);

        userTexto   = Objects.requireNonNull(getIntent().getExtras()).getString("USER","defaultUser");
        currentUser.setText(userTexto);

        // SECCION listView ------------------------------------------------------------------------

        listaAutos  = findViewById(R.id.GRAFlista);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SecondActivity.this);
        String marca = prefs.getString("marca", "TSL");
        Integer imgID = R.drawable.logo_default;

        ArrayList<String> datos = new ArrayList<>();

        if ( marca.equals("TSL") )
        {
            imgID = R.drawable.logo_tesla;
            SQLiteDatabase db = usdbh.getWritableDatabase();

            //Si hemos abierto correctamente la base de datos
            if(db != null)
            {
                Cursor c = db.rawQuery("SELECT modelo FROM Car WHERE modelo LIKE '%tesla%'",null);
                if(c.moveToFirst())
                {
                    int nRecords = c.getCount();
                    for(int i=0;i<nRecords;i++)
                    {
                        datos.add(c.getString(0));
                        c.moveToNext();
                    }
                }
                c.close();

                //Cerramos la base de datos
                db.close();
            }
        }
        else if ( marca.equals("NTSL") )
        {
            SQLiteDatabase db = usdbh.getWritableDatabase();

            //Si hemos abierto correctamente la base de datos
            if(db != null)
            {
                Cursor c = db.rawQuery("SELECT modelo FROM Car WHERE modelo NOT LIKE '%tesla%'",null);
                if(c.moveToFirst())
                {
                    int nRecords = c.getCount();
                    for(int i=0;i<nRecords;i++)
                    {
                        datos.add(c.getString(0));
                        c.moveToNext();
                    }
                }
                c.close();

                //Cerramos la base de datos
                db.close();
            }
        }
        else
        {
            SQLiteDatabase db = usdbh.getWritableDatabase();

            //Si hemos abierto correctamente la base de datos
            if(db != null)
            {
                Cursor c = db.rawQuery("SELECT modelo FROM Car WHERE modelo LIKE '%tesla%'",null);
                if(c.moveToFirst())
                {
                    int nRecords = c.getCount();
                    for(int i=0;i<nRecords;i++)
                    {
                        datos.add(c.getString(0));
                        c.moveToNext();
                    }
                }
                c.close();

                //Cerramos la base de datos
                db.close();
            }
        }

        CustomListAdapter adaptadorAutos = new CustomListAdapter(this, datos, imgID);
        listaAutos.setAdapter(adaptadorAutos);
        registerForContextMenu(listaAutos);

        // endSECCION listView ---------------------------------------------------------------------

        listaAutos.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> a, View v, int position, long id)
            {
                String selectedCar = listaAutos.getItemAtPosition(position).toString();
                selectedCar = '"' + selectedCar + '"';
                SQLiteDatabase db = usdbh.getWritableDatabase();
                Integer selectedCarCode = 0;

                //Si hemos abierto correctamente la base de datos
                if(db != null)
                {
                    Cursor c = db.rawQuery("SELECT codigo_car FROM Car WHERE modelo=" + selectedCar,null);
                    if(c.moveToFirst())
                    {
                        selectedCarCode = c.getInt(0);
                    }
                    c.close();

                    //Cerramos la base de datos
                    db.close();
                }

                Intent Int3 = new Intent(SecondActivity.this,CarTabsActivity.class);
                Int3.putExtra("CAR",selectedCarCode);
                startActivity(Int3);
            }
        });

        btnFloatAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                refreshRequired = true;
                Intent Int5 = new Intent(SecondActivity.this,AddActivity.class);
                startActivity(Int5);
            }
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

        menu.setHeaderTitle(listaAutos.getAdapter().getItem(info.position).toString());

        inflater.inflate(R.menu.menu_ctx_lista, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        String selectedCar = listaAutos.getItemAtPosition(info.position).toString();
        selectedCar = '"' + selectedCar + '"';
        SQLiteDatabase db = usdbh.getWritableDatabase();
        Integer selectedCarCode = 1;

        //Si hemos abierto correctamente la base de datos
        if(db != null)
        {
            Cursor c = db.rawQuery("SELECT codigo_car FROM Car WHERE modelo=" + selectedCar,null);
            if(c.moveToFirst())
            {
                selectedCarCode = c.getInt(0);
            }
            c.close();

            switch (item.getItemId())
            {
                case R.id.CtxLstOpc1:
                    Intent Int4 = new Intent(SecondActivity.this,ModifActivity.class);
                    Int4.putExtra("item",selectedCarCode);
                    startActivity(Int4);
                    // Cerramos la DB
                    db.close();
                    return true;
                case R.id.CtxLstOpc2:
                    db.execSQL("DELETE FROM Car WHERE codigo_car=" + selectedCarCode);
                    db.execSQL("DELETE FROM CarDetail WHERE codigo_car=" + selectedCarCode);
                    // Cerramos la DB
                    db.close();
                    finish();
                    startActivity(getIntent());
                    return true;
                default:
                    // Cerramos la DB
                    db.close();
                    return super.onContextItemSelected(item);
            }
        }
        return true;
    }
}