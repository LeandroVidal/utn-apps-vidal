package utn.lmv.p001;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

public class ModifActivity extends AppCompatActivity
{
    public EditText edtACCEL;
    public EditText edtPOWER;
    public EditText edtSPEED;
    public EditText edtRANGE;
    public EditText edtPRICE;

    public TextView txtCarModel;

    public Button btnModify;

    public String accel_value = "";
    public String power_value = "";
    public String speed_value = "";
    public String range_value = "";
    public String price_value = "";

    //Abrimos la base de datos 'DB_LMV' en modo escritura
    public UsuariosSQLittleHelper usdbh = new UsuariosSQLittleHelper(this, "DB_LMV", null, 1);
    public Integer selectedCarCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif);

        edtACCEL = findViewById(R.id.GRAFEdtACCEL);
        edtPOWER = findViewById(R.id.GRAFEdtPOWER);
        edtSPEED = findViewById(R.id.GRAFEdtSPEED);
        edtRANGE = findViewById(R.id.GRAFEdtRANGE);
        edtPRICE = findViewById(R.id.GRAFEdtPRICE);

        txtCarModel = findViewById(R.id.GRAFtxtView);

        btnModify = findViewById(R.id.GRAFbtnModif);

        selectedCarCode = Objects.requireNonNull(getIntent().getExtras()).getInt("item",1);

        SQLiteDatabase db = usdbh.getWritableDatabase();

        // Si hemos abierto correctamente la base de datos
        if(db != null)
        {

            Cursor c = db.rawQuery("SELECT modelo FROM Car WHERE codigo_car=" + selectedCarCode,null);
            if(c.moveToFirst())
            {
                txtCarModel.setText(c.getString(0));
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Car not found!", Toast.LENGTH_SHORT).show();
            }
            c.close();

            c = db.rawQuery("SELECT accel,power,top_speed,range,price FROM CarDetail WHERE codigo_car=" + selectedCarCode,null);
            if(c.moveToFirst())
            {
                edtACCEL.setText(c.getString(0));
                edtPOWER.setText(c.getString(1));
                edtSPEED.setText(c.getString(2));
                edtRANGE.setText(c.getString(3));
                edtPRICE.setText(c.getString(4));
            }
            c.close();

            //Cerramos la base de datos
            db.close();
        }

        btnModify.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                accel_value = '"' + edtACCEL.getText().toString() + '"';
                power_value = '"' + edtPOWER.getText().toString() + '"';
                speed_value = '"' + edtSPEED.getText().toString() + '"';
                range_value = '"' + edtRANGE.getText().toString() + '"';
                price_value = '"' + edtPRICE.getText().toString() + '"';

                SQLiteDatabase db = usdbh.getWritableDatabase();

                if( accel_value.length() == 2 || power_value.length() == 2 || speed_value.length() == 2 || range_value.length() == 2 || price_value.length() == 2  )
                {
                    Toast.makeText(getApplicationContext(), "All fields must be completed", Toast.LENGTH_SHORT).show();
                }
                else
                {

                    //Si hemos abierto correctamente la base de datos
                    if (db != null)
                    {
                        db.execSQL("UPDATE CarDetail SET   accel = " + accel_value +
                                ", power = " + power_value +
                                ", top_speed = " + speed_value +
                                ", range = " + range_value +
                                ", price = " + price_value +
                                "WHERE codigo_car=" + selectedCarCode);

                        //Cerramos la base de datos
                        db.close();
                    }
                    Toast.makeText(getApplicationContext(), "Car details successfully updated", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}
