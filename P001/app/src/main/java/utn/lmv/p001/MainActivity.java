package utn.lmv.p001;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    public Button Boton1;
    public EditText Edit1;
    public String TextoIngresado;
    //Abrimos la base de datos 'DBUsuarios' en modo escritura
    public UsuariosSQLittleHelper usdbh = new UsuariosSQLittleHelper(this, "DB_LMV", null, 1);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteDatabase db = usdbh.getWritableDatabase();
        db.execSQL("DELETE FROM Usuarios");
        db.execSQL("DELETE FROM Car");
        db.execSQL("DELETE FROM CarDetail");

        //Si hemos abierto correctamente la base de datos
        if(db != null)
        {
            //Insertamos 5 usuarios de ejemplo
            for(int i=1; i<=5; i++)
            {
                //Generamos los datos
                String nombre = "User" + i;

                //Insertamos los datos en la tabla Usuarios
                db.execSQL("INSERT INTO Usuarios (codigo, nombre) VALUES (" + i + ", '" + nombre +"')");
            }

            String datos[] = {"Tesla Model 3", "Tesla Model X", "Tesla Model S", "Tesla Roadster", "Tesla Semi", "Porsche Mission E", "Audi E-Tron Sportback", "BMW i3", "Nissan Leaf", "Renault Twizy Urban 80"};

            for(int i=1; i<11; i++)
            {
                db.execSQL("INSERT INTO Car (codigo_car, modelo) VALUES (" + i + ", '" + datos[i-1] +"')");
            }

            String datos_accel[] = {"5.1sec","3.3sec","2.8sec","1.9sec","20sec (fully loaded)","3.5sec","6.9sec","6.8sec","7.4sec","6.0sec"};
            String datos_power[] = {"271HP","518HP","518HP","1000HP","1000HP","600HP","500HP","170HP","147HP","17HP"};
            String datos_top_speed[] = {"141mph","130mph","155mph","250mph","60mph","155mph","138mph","100mph","92mph","50mph"};
            String datos_range[] = {"310 miles","265 miles","337 miles","620 miles","400 miles","300 miles","250 miles","180 miles","151 miles","50 miles"};
            String datos_price[] = {"50.200usd","80.700usd","105.670usd","200.000usd","150.000usd", "85.000usd","78.000usd","45.445usd","30.885usd","7.700usd"};

            for(int i=1; i<11; i++)
            {
                db.execSQL("INSERT INTO CarDetail (codigo_car,accel,power,top_speed,range,price) VALUES (" + i + ", '" + datos_accel[i-1] +"', '" + datos_power[i-1] +"', '" + datos_top_speed[i-1] +"', '" + datos_range[i-1] +"', '" + datos_price[i-1] +"')");
            }

            //Cerramos la base de datos
            db.close();
        }

        Boton1 = findViewById(R.id.Btn1);
        Edit1 = findViewById(R.id.Edt1);


        Boton1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                TextoIngresado = '"' + Edit1.getText().toString() + '"';

                SQLiteDatabase db = usdbh.getWritableDatabase();

                //Si hemos abierto correctamente la base de datos
                if(db != null)
                {
                    Cursor c = db.rawQuery("SELECT nombre FROM Usuarios WHERE nombre=" + TextoIngresado,null);

                    //Nos aseguramos de que existe al menos un registro
                    if (c.moveToFirst())
                    {
                        //Recorremos el cursor hasta que no haya más registros
                        String nombreUser = c.getString(0);
                        c.close();

                        Intent Int1 = new Intent(MainActivity.this,SecondActivity.class);
                        Int1.putExtra("USER",nombreUser);
                        startActivity(Int1);
                    }
                    else
                    {
                        c.close();
                        Context context = getApplicationContext();
                        Toast toast = Toast.makeText(context, "Invalid User " + TextoIngresado, Toast.LENGTH_SHORT);
                        toast.show();
                    }

                    //Cerramos la base de datos
                    db.close();
                }
            }
        });
    }
}
