package utn.lmv.p002;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CarC_Fragment extends Fragment
{

    public TextView txtAsync;
    public String server_responsePROCfrag = CarTabsActivity.server_responsePROC;

//    public int notificationId = 1;
//    public String CHANNEL_ID = "my_channel_01";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_car_c, container, false);

        txtAsync = view.findViewById(R.id.GRAFtxtAsync);
        txtAsync.setText(server_responsePROCfrag);

        new CountDownTimer(1000, 1000)
        {
            public void onFinish()
            {
                server_responsePROCfrag = CarTabsActivity.server_responsePROC;
                txtAsync.setText("rate: " + server_responsePROCfrag);
                this.start();
            }
            public void onTick(long millisUntilFinished)
            {
            }
        }.start();

        return view;
    }
}
