package utn.lmv.p002;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class CarA_Fragment extends Fragment
{
    public TextView carModel;
    public ImageView carImageView;
    public String selectedCar = CarTabsActivity.selectedCar;
    public String selectedImg = CarTabsActivity.selectedCarImg;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_car_a, container, false);

        carModel = view.findViewById(R.id.GRAFcarModel);
        carImageView = view.findViewById(R.id.GRAFcarImage);

        PhotoViewAttacher photoView = new PhotoViewAttacher (carImageView);
        photoView.update();

        // Seteo texto con el modelo del auto
        carModel.setText(selectedCar);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://doparcial-f3cbc.appspot.com");
        StorageReference pathReference = storageRef.child("images/" + selectedImg);

        GlideApp.with( getActivity() )
                .load( pathReference )
                .into( carImageView );

        return view;
    }
}
