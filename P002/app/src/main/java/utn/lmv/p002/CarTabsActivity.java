package utn.lmv.p002;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CarTabsActivity extends AppCompatActivity
{
    public static String selectedCar = "DEFAULT";
    public static String[] valuesSpin = new String[5];
    public static Integer selectedCarCode = 1;
    public static String selectedCarImg = "";
    public static String unitSystem = "";
    public static String currency = "";

    public static boolean f_ERROR = false;
    public String server_response = null;
    public float USDoriginal = 0;
    public static String server_responsePROC = "";

    public ViewPagerAdapter mViewPagerAdapter;
    public ViewPager mViewPager;
    public TabLayout tabLayout;

    public CountDownTimer myTimer;

    public DatabaseReference firebaseReference;

    public int notificationId = 1;
    public String CHANNEL_ID = "my_channel_01";

    public CarTabsActivity()
    {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_tabs);

        mViewPagerAdapter = new ViewPagerAdapter( getSupportFragmentManager() );

        mViewPager = findViewById(R.id.user_pager);

        selectedCarCode = Objects.requireNonNull(getIntent().getExtras()).getInt("CAR",0);
        selectedCar = Objects.requireNonNull(getIntent().getExtras()).getString("CARMODEL","Audi E-Tron Sportback");
        selectedCarImg = Objects.requireNonNull(getIntent().getExtras()).getString("IMG","electric_default.jpg");
        String selectedCarBrand = Objects.requireNonNull(getIntent().getExtras()).getString("BRAND","other");
        unitSystem = Objects.requireNonNull(getIntent().getExtras()).getString("UNITS","imperial");
        currency = Objects.requireNonNull(getIntent().getExtras()).getString("CURRENCY","USD");

        firebaseReference = FirebaseDatabase.getInstance().getReference();

        firebaseReference.child("cars").child(selectedCarBrand).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                {
                    if (modelSnapshot.child("ID").getValue(Integer.class).equals(selectedCarCode))
                    {
                        valuesSpin[0] = "0-60mph Acceleration Time: " + modelSnapshot.child("accel").getValue(String.class);
                        valuesSpin[1] = "Power: " + modelSnapshot.child("power").getValue(String.class);
                        valuesSpin[2] = "Top Speed: " + modelSnapshot.child("top_speed").getValue(String.class);
                        valuesSpin[3] = "Range: " + modelSnapshot.child("range").getValue(String.class);
                        valuesSpin[4] = "Base Price: " + modelSnapshot.child("price").getValue(String.class);

                        USDoriginal = Float.parseFloat( valuesSpin[4].replace("Base Price: ", "").replace("usd", "").replace("ars", "").replace(".", "") );

                        // Buscar rate por primera vez, ya que el timer va a tardar 10seg
                        if (currency.equals("ARS"))
                        {
                            new CarTabsActivity.GetMethodDemo().execute("http://ws.geeklab.com.ar/dolar/dolar-iframe.php");
                            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(CarTabsActivity.this, CHANNEL_ID)
                                    .setSmallIcon(R.drawable.small_icon)
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.logo_default))
                                    .setContentTitle("P002 EV")
                                    .setContentText("Searching for usd/ars rate")
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                    // Set the intent that will fire when the user taps the notification
                                    .setAutoCancel(true);

                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from( getApplicationContext() );
                            notificationManager.notify( notificationId, mBuilder.build() );
                        }

                        if (unitSystem.equals("metric"))
                        {
                            valuesSpin[0] = valuesSpin[0].replace("60mph", "100km/h");

                            valuesSpin[2] = valuesSpin[2].replace("mph", "").replace("Top Speed: ", "");
                            Float convertNumber = Float.parseFloat(valuesSpin[2]) * 1.6f;
                            valuesSpin[2] = "Top Speed: " + String.format("%.2f", convertNumber) + "km/h";

                            valuesSpin[3] = valuesSpin[3].replace(" miles", "").replace("Range: ", "");
                            convertNumber = Float.parseFloat(valuesSpin[3]) * 1.6f;
                            valuesSpin[3] = "Range: " + String.format("%.0f", convertNumber) + " km";
                        }

                        setupViewPager(mViewPager);

                        tabLayout = findViewById(R.id.user_tabs);
                        tabLayout.setupWithViewPager(mViewPager);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError firebaseError)
            {
                Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        myTimer = new CountDownTimer(30000, 1000)
        {
            boolean priceRefreshRequired = true;

            public void onFinish()
            {
                priceRefreshRequired = true;
                new CarTabsActivity.GetMethodDemo().execute("http://ws.geeklab.com.ar/dolar/dolar-iframe.php");

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(CarTabsActivity.this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.small_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.logo_default))
                        .setContentTitle("P002 EV")
                        .setContentText("Searching for usd/ars rate")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setAutoCancel(true);

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from( getApplicationContext() );
                notificationManager.notify( notificationId, mBuilder.build() );

                this.start();
            }

            public void onTick(long millisUntilFinished)
            {
//                Log.d("TABS", "Restan para los 10000: " + millisUntilFinished);
                if (millisUntilFinished < 25000 && priceRefreshRequired)
                {
                    priceRefreshRequired = false;

                    if (f_ERROR)
                    {
                        f_ERROR = false;
                        Toast.makeText(getApplicationContext(), "Fetch USD/ARS rate FAILED", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if (currency.equals("ARS"))
                        {
//                            Toast.makeText(getApplicationContext(), "Rate obtained: " + server_responsePROC, Toast.LENGTH_SHORT).show();

                            Float convertNumber = USDoriginal * Float.parseFloat(server_responsePROC);
                            valuesSpin[4] = "Base Price: " + String.format("%.0f", convertNumber) + "ars";

                            createNotificationChannel();

                            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(CarTabsActivity.this, CHANNEL_ID)
                                    .setSmallIcon(R.drawable.small_icon)
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.logo_default))
                                    .setContentTitle("P002 EV")
                                    .setContentText("Found usd/ars rate: $" + server_responsePROC)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                    // Set the intent that will fire when the user taps the notification
                                    .setAutoCancel(true);

                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from( getApplicationContext() );
                            notificationManager.notify( notificationId, mBuilder.build() );

                        }
                    }
                }
            }
        }.start();
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter( getSupportFragmentManager() );

        adapter.addFragment(new CarA_Fragment(), "CAR OVERVIEW");
        adapter.addFragment(new CarB_Fragment(), "CAR SPECS");
        adapter.addFragment(new CarC_Fragment(), "CURRENCY");


        viewPager.setAdapter(adapter);
    }

    public class GetMethodDemo extends AsyncTask<String , Void ,String>
    {

        @Override
        protected String doInBackground(String... strings)
        {

            URL url;
            HttpURLConnection urlConnection;

            try
            {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK)
                {
                    server_response = readStream(urlConnection.getInputStream());
                }

                Matcher m = Pattern.compile("[0-9]+\\.[0-9]+",Pattern.MULTILINE).matcher(server_response); // ORIG: AR\$\s[0-9]+\.[0-9]+

                while ( m.find() )
                {
                    server_responsePROC = m.group();
                }
            }
            catch (MalformedURLException e)
            {
                f_ERROR = true;
            }
            catch (IOException e)
            {
                f_ERROR = true;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
        }
    }

// Converting InputStream to String

    private String readStream(InputStream in)
    {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();

        try
        {
            reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null)
            {
                response.append(line + "\n");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    @Override
    public void onBackPressed()
    {
        myTimer.cancel();
        myTimer = null;
        valuesSpin[4] = "Base Price: " + String.format("%.0f", USDoriginal) + "usd";
        finish();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            CharSequence name = "MyChannel";
            String description = "NotifyRate";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}