package utn.lmv.p002;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class ModifActivity extends AppCompatActivity
{
    public EditText edtACCEL;
    public EditText edtPOWER;
    public EditText edtSPEED;
    public EditText edtRANGE;
    public EditText edtPRICE;

    public TextView txtCarModel;

    public Button btnModify;

    public String accel_value = "";
    public String power_value = "";
    public String speed_value = "";
    public String range_value = "";
    public String price_value = "";

    public DatabaseReference firebaseReference;

    public static Integer selectedCarCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif);

        edtACCEL = findViewById(R.id.GRAFEdtACCEL);
        edtPOWER = findViewById(R.id.GRAFEdtPOWER);
        edtSPEED = findViewById(R.id.GRAFEdtSPEED);
        edtRANGE = findViewById(R.id.GRAFEdtRANGE);
        edtPRICE = findViewById(R.id.GRAFEdtPRICE);

        txtCarModel = findViewById(R.id.GRAFmodelTxtView);

        btnModify = findViewById(R.id.GRAFbtnModif);

        selectedCarCode = Objects.requireNonNull(getIntent().getExtras()).getInt("CAR",1);
        String selectedCarBrand = Objects.requireNonNull(getIntent().getExtras()).getString("BRAND","other");

        firebaseReference = FirebaseDatabase.getInstance().getReference().child("cars").child(selectedCarBrand);

        firebaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                for ( DataSnapshot modelSnapshot : dataSnapshot.getChildren() )
                {
                    if ( modelSnapshot.child("ID").getValue(Integer.class).equals(selectedCarCode) )
                    {
                        txtCarModel.setText(modelSnapshot.child("model").getValue(String.class));

                        edtACCEL.setText(modelSnapshot.child("accel").getValue(String.class));
                        edtPOWER.setText(modelSnapshot.child("power").getValue(String.class));
                        edtSPEED.setText(modelSnapshot.child("top_speed").getValue(String.class));
                        edtRANGE.setText(modelSnapshot.child("range").getValue(String.class));
                        edtPRICE.setText(modelSnapshot.child("price").getValue(String.class));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError firebaseError)
            {
                Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        btnModify.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                accel_value = edtACCEL.getText().toString();
                power_value = edtPOWER.getText().toString();
                speed_value = edtSPEED.getText().toString();
                range_value = edtRANGE.getText().toString();
                price_value = edtPRICE.getText().toString();

                if( accel_value.length() == 2 || power_value.length() == 2 || speed_value.length() == 2 || range_value.length() == 2 || price_value.length() == 2  )
                {
                    Toast.makeText(getApplicationContext(), "All fields must be completed", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    firebaseReference.addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {
                            for ( DataSnapshot modelSnapshot : dataSnapshot.getChildren() )
                            {
                                if ( modelSnapshot.child("ID").getValue(Integer.class).equals(selectedCarCode))
                                {
                                    modelSnapshot.getRef().child("accel").setValue(accel_value);
                                    modelSnapshot.getRef().child("power").setValue(power_value);
                                    modelSnapshot.getRef().child("top_speed").setValue(speed_value);
                                    modelSnapshot.getRef().child("range").setValue(range_value);
                                    modelSnapshot.getRef().child("price").setValue(price_value);

                                    Toast.makeText(getApplicationContext(), "Car successfully modified", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError firebaseError)
                        {
                            Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
            }
        });
    }
}
