package utn.lmv.p002;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;

public class AddActivity extends AppCompatActivity
{
    public EditText edtMODEL;
    public EditText edtACCEL;
    public EditText edtPOWER;
    public EditText edtSPEED;
    public EditText edtRANGE;
    public EditText edtPRICE;

    public String model_value = "";
    public String accel_value = "";
    public String power_value = "";
    public String speed_value = "";
    public String range_value = "";
    public String price_value = "";

    public static final int PICK_IMAGE = 1;

    public String model_value_key = "";

    public TextView txtCarModel;

    public Button btnAdd;

    public DatabaseReference firebaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        edtMODEL = findViewById(R.id.GRAFEdtMODEL);
        edtACCEL = findViewById(R.id.GRAFEdtACCEL);
        edtPOWER = findViewById(R.id.GRAFEdtPOWER);
        edtSPEED = findViewById(R.id.GRAFEdtSPEED);
        edtRANGE = findViewById(R.id.GRAFEdtRANGE);
        edtPRICE = findViewById(R.id.GRAFEdtPRICE);

        txtCarModel = findViewById(R.id.GRAFtxtView);

        btnAdd = findViewById(R.id.GRAFbtnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                model_value = edtMODEL.getText().toString();

                accel_value = edtACCEL.getText().toString();
                power_value = edtPOWER.getText().toString();
                speed_value = edtSPEED.getText().toString();
                range_value = edtRANGE.getText().toString();
                price_value = edtPRICE.getText().toString();

                model_value_key = model_value.toLowerCase().replaceAll(" ","") + System.currentTimeMillis();

                String selectedCarBrand = Objects.requireNonNull(getIntent().getExtras()).getString("BRAND","other");

                firebaseReference = FirebaseDatabase.getInstance().getReference().child("cars").child(selectedCarBrand);

                if( model_value.length() == 2 || accel_value.length() == 2 || power_value.length() == 2 || speed_value.length() == 2 || range_value.length() == 2 || price_value.length() == 2  )
                {
                    Toast.makeText(getApplicationContext(), "All fields must be completed", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    // Averiguamos el ID mas alto de la marca
                    firebaseReference.orderByChild("ID").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {
                            for ( DataSnapshot idSnapshot : dataSnapshot.getChildren() )
                            {
                                Integer id_value = idSnapshot.child("ID").getValue(Integer.class);

                                int id_value_int = id_value.intValue() + 1;

                                // Insertamos el ID mas alto +1
                                firebaseReference.child(model_value_key).child("ID").setValue(id_value_int);

                                firebaseReference.child(model_value_key).child("model").setValue(model_value);

                                firebaseReference.child(model_value_key).child("accel").setValue(accel_value);
                                firebaseReference.child(model_value_key).child("power").setValue(power_value);
                                firebaseReference.child(model_value_key).child("top_speed").setValue(speed_value);
                                firebaseReference.child(model_value_key).child("range").setValue(range_value);
                                firebaseReference.child(model_value_key).child("price").setValue(price_value);
                                firebaseReference.child(model_value_key).child("img").setValue(model_value_key);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError firebaseError)
                        {
                        }
                    });

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            switch (which)
                            {
                                case DialogInterface.BUTTON_POSITIVE:

                                    // REFERENCIA AL STORAGE
//                                    FirebaseStorage storage = FirebaseStorage.getInstance();
//                                    StorageReference storageRef = storage.getReferenceFromUrl("gs://doparcial-f3cbc.appspot.com");
//                                    StorageReference pathReference = storageRef.child("images/" + model_value_key);

                                    // ABRIR SELECCION DE ARCHIVO (WIP)
                                    chooseImage();

//                                    // SUBIR IMAGEN DE PRUEBA DE DRAWABLES (WIP)
//                                    Bitmap bitmap = BitmapFactory.decodeResource(getApplication().getResources(), R.drawable.logo_default);
//                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//                                    byte[] data = baos.toByteArray();
//
//                                    pathReference.putBytes(data);
//
//
//
//
//
//
//
//
//                                    Toast.makeText(getApplicationContext(), "New car successfully added", Toast.LENGTH_SHORT).show();
//                                    finish();
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    Toast.makeText(getApplicationContext(), "New car successfully added", Toast.LENGTH_SHORT).show();
                                    finish();
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder( v.getContext() );
                    builder.setMessage("Do you want to attach an image to this car?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
                }
            }
        });
    }

    private void chooseImage()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent dataIMG)
    {
        super.onActivityResult(requestCode, resultCode, dataIMG);
        if (requestCode == PICK_IMAGE)
        {
            // REFERENCIA AL STORAGE
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl("gs://doparcial-f3cbc.appspot.com");
            StorageReference pathReference = storageRef.child("images/" + model_value_key);

            // SUBIR IMAGEN DE PRUEBA DE DRAWABLES (WIP)
            Uri uriIMG = dataIMG.getData();
            Bitmap bitmap = null;
            try
            {
                bitmap = MediaStore.Images.Media.getBitmap( this.getContentResolver(), uriIMG );
            } catch (IOException e)
            {
                e.printStackTrace();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            pathReference.putBytes(data);

            Toast.makeText(getApplicationContext(), "New car successfully added", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

}
