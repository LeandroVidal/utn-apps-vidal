package utn.lmv.p002;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class CarB_Fragment extends Fragment
{
    public Spinner infoSpinner;
    public TextView description;
    public String[] valuesSpinComplete = CarTabsActivity.valuesSpin;
    public String[] valuesSpinner = new String[5];

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_car_b, container, false);

        infoSpinner = view.findViewById(R.id.GRAFspinner);
        description = view.findViewById(R.id.GRAFcarDesc);

        for( Integer i=0 ; i<5 ; i++ )
        {
            valuesSpinner[i] = valuesSpinComplete[i].replaceAll(":.*","");
        }

        ArrayAdapter<String> adaptadorSpin = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), android.R.layout.simple_spinner_item, valuesSpinner);
        infoSpinner.setAdapter(adaptadorSpin);

        infoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView adapter, View v, int position, long lng)
            {
                Integer selectedItemPosition = position;

                String specHuman = CarTabsActivity.valuesSpin[position].replace(": "," of ");

                description.setText("This EV has a " + specHuman + ".");

                switch (selectedItemPosition)
                {
                    case 0:
                        description.append("\n\nThis is the minimum time needed for the car to reach the mentioned speed, as tested in straight drag line by verified contributions.");
                        break;
                    case 1:
                        description.append("\n\nThis is the torque level the electric motor can develop, as informed by manufacturers.");
                        break;
                    case 2:
                        description.append("\n\nThis is the top reachable speed for this model, as tested in straight drag line by verified contributions.");
                        break;
                    case 3:
                        description.append("\n\nThis is the driving range of a vehicle using only power from its electric battery pack to traverse a given driving cycle. It means the total range per charge.");
                        break;
                    case 4:
                        description.append("\n\nThis is the price for the most basic version of this model.");
                        break;
                    default:
                        description.append("\n\nEV cars and trucks make a huge positive impact on environment. Please, reconsider before your next car purchase!");
                }

                if (selectedItemPosition >= 0 && selectedItemPosition < 5)
                {
                    description.append("\n\nEV cars and trucks make a huge positive impact on environment. Please, reconsider before your next car purchase!");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {

            }
        });

        return view;
    }
}
