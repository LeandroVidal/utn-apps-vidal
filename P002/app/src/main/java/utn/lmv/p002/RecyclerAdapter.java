package utn.lmv.p002;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CarViewHolder> implements View.OnClickListener
{
        private ArrayList<String> datos;
        private final Integer imgid;
        private View.OnClickListener listener;
        public int position;

        public int getPosition()
        {
            return position;
        }

        public void setPosition(int position)
        {
            this.position = position;
        }

        public RecyclerAdapter(ArrayList<String> datos, Integer imgid)
        {
            this.datos = datos;
            this.imgid = imgid;
        }

        @Override
        public void onViewRecycled(CarViewHolder holder)
        {
            holder.itemView.setOnLongClickListener(null);
            super.onViewRecycled(holder);
        }

        @Override
        public @NonNull CarViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
        {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_format, viewGroup, false);

            itemView.setOnClickListener(this);

            return new CarViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final CarViewHolder viewHolder, int pos)
        {
            String item1 = datos.get(pos);

            viewHolder.bindTitular(item1,imgid);

            // AGREGADO CTX MENU
            viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View v)
                {
                    position = viewHolder.getAdapterPosition();
                    setPosition(position);
                    return false;
                }
            });
        }

        @Override
        public int getItemCount()
        {
            return datos.size();
        }

        // CLASE ADAPTER

        public static class CarViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener
        {
            private TextView carModel;
            private ImageView carIcon;

            private CarViewHolder(View itemView)
            {
                super(itemView);

                carModel = itemView.findViewById(R.id.Itemname);
                carIcon = itemView.findViewById(R.id.icon);
                itemView.setOnCreateContextMenuListener(this);
            }

            private void bindTitular(String car, Integer icon)
            {
                carModel.setText(car);
                carIcon.setImageResource(icon);
            }

            // AGREGADO CTX MENU
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
            {
                //menuInfo is null
                menu.setHeaderTitle("Select action");
                menu.add(Menu.NONE, R.id.CtxLstOpc1, Menu.NONE, "Modify");
                menu.add(Menu.NONE, R.id.CtxLstOpc2, Menu.NONE, "Delete");
            }
        }

        // ONCLICK LISTENER

        public void setOnClickListener(View.OnClickListener listener)
        {
            this.listener = listener;
        }

        @Override
        public void onClick(View view)
        {
            if(listener != null)
                listener.onClick(view);
        }
}