package utn.lmv.p002;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity
{
    public Boolean refreshRequired = false;
    public Boolean modifyRequired = false;
    public Boolean deleteRequired = false;
    public static Boolean skipRequired = false;
    public TextView currentUser;
    public TextView textInstruction;
    public FloatingActionButton btnFloatAdd;
    public RecyclerView recView;

    public FirebaseUser firebaseUser;
    public DatabaseReference firebaseReference;

    public ArrayList<String> datos = new ArrayList<>();
    public String selectedCar = "";
    public Integer selectedCarCode = 1;
    public String selectedCarImg = "";
    public String brand = "";
    public String unit = "";
    public String currency = "";
    public String fireBaseBrand = "";

    public CountDownTimer myTimerSecond;

    public RecyclerAdapter adaptador;

    public Boolean timerToggle = false;

    public SharedPreferences prefs;

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_settings:
                refreshRequired = true;
                Intent Int2 = new Intent(SecondActivity.this,SettingsActivity.class);
                startActivity(Int2);
                return true;
            case R.id.action_test:
                finish();
                startActivity(getIntent());
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Esto es para forzar a que se regenere el RecyclerView despues de modificar las opciones
    @Override
    protected void onResume()
    {
        super.onResume();

        if (skipRequired)
        {
            skipRequired = false;
        }

        if (refreshRequired)
        {
            refreshRequired = false;
            finish();
            startActivity(getIntent());
        }
    }

//---ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY------ACTIVITY---

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Toolbar myToolbar = findViewById(R.id.GRAFtoolbar);
        setSupportActionBar(myToolbar);

        currentUser = findViewById(R.id.GRAFcurrentUser);
        btnFloatAdd = findViewById(R.id.GRAFbtnFloat);
        textInstruction = findViewById(R.id.GRAFdynAD);

// FIREBASE FIREBASE FIREBASE FIREBASE FIREBASE FIREBASE FIREBASE FIREBASE FIREBASE FIREBASE FIREBASE

        firebaseReference = FirebaseDatabase.getInstance().getReference();

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        currentUser.setText( "User: " + firebaseUser.getEmail().split("@")[0] );

// endFIREBASE endFIREBASE endFIREBASE endFIREBASE endFIREBASE endFIREBASE endFIREBASE endFIREBASE

// SECCION RecyclerView ----------------------------------------------------------------------------

        // READ PREFERENCES
        prefs = PreferenceManager.getDefaultSharedPreferences(SecondActivity.this);

        brand = prefs.getString("brand", "TSL");
        unit = prefs.getString("unit", "imperial");
        currency = prefs.getString("currency", "USD");
        modifyRequired = prefs.getBoolean("modifyReq",false);
        deleteRequired = prefs.getBoolean("deleteReq",false);

        Integer imgID = R.drawable.logo_default;

        if ( brand.equals("TSL") )
            fireBaseBrand = "tesla";
        else if ( brand.equals("NTSL") )
            fireBaseBrand = "other";

        switch ( brand )
        {
            case "TSL":
                imgID = R.drawable.logo_tesla;

                firebaseReference.child("cars").child("tesla").orderByChild("model").addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                        {
                            datos.add( modelSnapshot.child("model").getValue(String.class) );
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError firebaseError)
                    {
                        Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
                break;
            case "NTSL":
                firebaseReference.child("cars").child("other").orderByChild("model").addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                        {
                            datos.add( modelSnapshot.child("model").getValue(String.class) );
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError firebaseError)
                    {
                        Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
                break;
            default:
                imgID = R.drawable.logo_tesla;

                firebaseReference.child("cars").child("tesla").orderByChild("model").addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                        {
                            datos.add( modelSnapshot.child("model").getValue(String.class) );
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError firebaseError)
                    {
                        Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
        }

        //Inicialización RecyclerView
        recView = findViewById(R.id.GRAFRecView);
        recView.setHasFixedSize(true);

        adaptador = new RecyclerAdapter(datos,imgID);

        if ( !modifyRequired && !deleteRequired )
        {
            // CASO NORMAL
            textInstruction.setText("Pick a Car!");

            adaptador.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    int recPosition = recView.getChildAdapterPosition(v);
                    selectedCar = ((TextView) recView.findViewHolderForAdapterPosition(recPosition).itemView.findViewById(R.id.Itemname)).getText().toString();

                    firebaseReference.child("cars").child(fireBaseBrand).orderByChild("ID").addValueEventListener(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {
                            if (!skipRequired)
                            {
                                for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                                {
                                    if ( modelSnapshot.child("model").getValue(String.class).equals(selectedCar) )
                                    {
                                        selectedCarCode = modelSnapshot.child("ID").getValue(Integer.class);
                                        selectedCarImg = modelSnapshot.child("img").getValue(String.class);

                                        Intent Int3 = new Intent(SecondActivity.this, CarTabsActivity.class);
                                        Int3.putExtra("CAR", selectedCarCode);
                                        Int3.putExtra("IMG", selectedCarImg);
                                        Int3.putExtra("BRAND", fireBaseBrand);
                                        Int3.putExtra("CARMODEL", selectedCar);
                                        Int3.putExtra("UNITS", unit);
                                        Int3.putExtra("CURRENCY", currency);
                                        startActivity(Int3);
                                    }
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError firebaseError)
                        {
                            Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
            });
        }
        else if ( modifyRequired )
        {
            // CASO MODIFY
            textInstruction.setText("Pick Car to MODIFY");

            adaptador.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    int recPosition = recView.getChildAdapterPosition(v);
                    selectedCar = ((TextView) recView.findViewHolderForAdapterPosition(recPosition).itemView.findViewById(R.id.Itemname)).getText().toString();

                    firebaseReference.child("cars").child(fireBaseBrand).addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {
                            for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                            {
                                if (modelSnapshot.child("model").getValue(String.class).equals(selectedCar))
                                {
                                    selectedCarCode = modelSnapshot.child("ID").getValue(Integer.class);

                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putBoolean("modifyReq",false);
                                    editor.putBoolean("deleteReq",false);
                                    editor.apply();

                                    Intent Int4 = new Intent(SecondActivity.this,ModifActivity.class);
                                    Int4.putExtra("CAR",selectedCarCode);
                                    Int4.putExtra("BRAND",fireBaseBrand);
                                    startActivity(Int4);
                                    refreshRequired = true;
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError firebaseError)
                        {
                            Toast.makeText(getApplicationContext(), "FireBase ERROR", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
            });
        }
        else if ( deleteRequired )
        {
            // CASO DELETE
            textInstruction.setText("Pick Car to DELETE");

            adaptador.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    int recPosition = recView.getChildAdapterPosition(v);
                    selectedCar = ((TextView) recView.findViewHolderForAdapterPosition(recPosition).itemView.findViewById(R.id.Itemname)).getText().toString();

                    firebaseReference.child("cars").child(fireBaseBrand).addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                        {
                            for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                            {
                                if (modelSnapshot.child("model").getValue(String.class).equals(selectedCar))
                                {
                                    modelSnapshot.getRef().setValue(null);

                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.putBoolean("deleteReq",false);
                                    editor.putBoolean("modifyReq",false);
                                    editor.apply();

                                    finish();
                                    startActivity(getIntent());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError firebaseError)
                        {

                        }
                    });
                }
            });
        }

        recView.setAdapter(adaptador);

        recView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        //recView.setLayoutManager(new GridLayoutManager(this,3));

        recView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        registerForContextMenu(recView);

// endSECCION RecyclerView -------------------------------------------------------------------------

// SECCION Timer ----------------------------------------------------------------------------
        myTimerSecond = new CountDownTimer(1000, 1000)
        {
            public void onFinish()
            {
                if( !timerToggle )
                {
                    ObjectAnimator colorAnim = ObjectAnimator.ofInt(textInstruction, "textColor",
                            getResources().getColor(R.color.colorTextSplash), getResources().getColor(R.color.colorAccent));
                    timerToggle = true;
                    colorAnim.setEvaluator(new ArgbEvaluator());
                    colorAnim.start();
                }
                else
                {
                    ObjectAnimator colorAnim = ObjectAnimator.ofInt(textInstruction, "textColor",
                            getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.colorTextSplash));
                    timerToggle = false;
                    colorAnim.setEvaluator(new ArgbEvaluator());
                    colorAnim.start();
                }
                this.start();
                if ( !deleteRequired && !modifyRequired )
                    adaptador.notifyDataSetChanged();
            }
            public void onTick(long millisUntilFinished)
            {
            }
        }.start();
// endSECCION Timer ----------------------------------------------------------------------------

        btnFloatAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                refreshRequired = true;
                skipRequired = true;
                Intent Int5 = new Intent(SecondActivity.this,AddActivity.class);
                Int5.putExtra("BRAND",fireBaseBrand);
                startActivity(Int5);
            }
        });

    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.CtxLstOpc1:
                SharedPreferences.Editor editor1 = prefs.edit();
                editor1.putBoolean("modifyReq",true);
                editor1.putBoolean("deleteReq",false);
                editor1.apply();
                skipRequired = true;
                finish();
                startActivity(getIntent());
                return true;
            case R.id.CtxLstOpc2:
                SharedPreferences.Editor editor2 = prefs.edit();
                editor2.putBoolean("deleteReq",true);
                editor2.putBoolean("modifyReq",false);
                editor2.apply();
                skipRequired = true;
                finish();
                startActivity(getIntent());
                return true;
        }
        return true;
    }
}