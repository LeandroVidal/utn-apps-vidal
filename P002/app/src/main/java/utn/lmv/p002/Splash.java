package utn.lmv.p002;

import android.content.Intent;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Splash extends AppCompatActivity
{

    public ConstraintLayout myLayout;
    public TextView tituloSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        myLayout    = findViewById(R.id.GRAFlayout);
        myLayout.setBackgroundColor(getResources().getColor(R.color.colorSplash));

        tituloSplash = findViewById(R.id.textView2);
        tituloSplash.setTextColor(0xFFAACC00);

        int secondsDelayed = 5;
        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                startActivity(new Intent(Splash.this, MainActivity.class));
                finish();
            }
        }, secondsDelayed * 1000);
    }
}
