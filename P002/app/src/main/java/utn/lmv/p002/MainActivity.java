package utn.lmv.p002;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity
{
    public Button Boton1;
    public EditText Edit1;
    public EditText Edit2;
    public String MailIngresado;
    public String PassIngresada;
    public ProgressDialog ProgDiag;

    public FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Boton1 = findViewById(R.id.Btn1);
        Edit1 = findViewById(R.id.Edt1);
        Edit2 = findViewById(R.id.Edt2);
        ProgDiag = new ProgressDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();

        Boton1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MailIngresado = Edit1.getText().toString().trim();
                PassIngresada = Edit2.getText().toString().trim();

                // Chequear que se hayan ingresado ambos campos
                if ( TextUtils.isEmpty(MailIngresado) || TextUtils.isEmpty(PassIngresada) )
                {
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context, "Please fill in both fields", Toast.LENGTH_SHORT);
                    toast.show();
                }
                else
                {
                    // Buscar usuarios y guardar resultado de la autenticacion
                    ProgDiag.setMessage("Signing in, please wait...");
                    ProgDiag.show();
                    firebaseAuth.signInWithEmailAndPassword(MailIngresado,PassIngresada)
                            .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>()
                            {

                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task)
                                {
                                    ProgDiag.dismiss();
                                    if( task.isSuccessful() )
                                    {
                                        // No es necesario pasarle info del User a la SecondActivity,
                                        //  ya que en esta haremos un getCurrentUser()
                                        Intent Int1 = new Intent(MainActivity.this,SecondActivity.class);
                                        startActivity( Int1 );
                                    }
                                    else
                                    {
                                        Context context = getApplicationContext();
                                        Toast toast = Toast.makeText(context, "Invalid Mail/Pass combination", Toast.LENGTH_SHORT);
                                        toast.show();
                                    }

                                }
                            });
                }

            }
        });

    }
}
